package com.afs.restapi.service;

import com.afs.restapi.entity.Company;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.exception.CompanyNotFoundException;
import com.afs.restapi.repository.JPACompanyRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompanyService {

    private final JPACompanyRepository jpaCompanyRepository;

    public CompanyService(JPACompanyRepository jpaCompanyRepository) {
        this.jpaCompanyRepository = jpaCompanyRepository;
    }


    public List<Company> findAll() {
        return jpaCompanyRepository.findAll();
    }

    public List<Company> findByPage(Integer page, Integer size) {
        PageRequest pageRequest = PageRequest.of(page - 1, size);
        Page<Company> companyPage = jpaCompanyRepository.findAll(pageRequest);
        return companyPage.getContent();
    }

    public Company findById(Long id) {
        return jpaCompanyRepository.findById(id).orElseThrow(CompanyNotFoundException::new);
    }

    public void update(Long id, Company company) {
//        Optional<Company> optionalCompany = jpaCompanyRepository.findById(id);
//        optionalCompany.ifPresent(previousCompany -> previousCompany.setName(company.getName()));
//        jpaCompanyRepository.save(optionalCompany.orElseThrow(CompanyNotFoundException::new));
        Company preCompany = findById(id);
        if (company.getName() != null) {
            preCompany.setName(company.getName());
        }
        jpaCompanyRepository.save(preCompany);
    }

    public Company create(Company company) {
        return jpaCompanyRepository.save(company);
    }

    public List<Employee> findEmployeesByCompanyId(Long id) {
//        return getEmployeeRepository().findByCompanyId(id);
        return jpaCompanyRepository.findById(id).map(Company::getEmployees).orElseThrow(CompanyNotFoundException::new);
    }

    public void delete(Long id) {
        jpaCompanyRepository.deleteById(id);
    }
}
